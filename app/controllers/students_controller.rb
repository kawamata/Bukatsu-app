class StudentsController < ApplicationController
  def index
    @students = Student.all
  end

  def show
    @student = Student.find(params[:id])
  end

  def new
    # @student = Student.new
  end

  def edit
    @student = Student.find(params[:id])
  end

  def create
    @student = Student.new(student_params)
    if @student.save
      flash[:success] = "部活登録完了！"
      redirect_to @student
    else
      flash[:warning] = "登録失敗"
      redirect_to new_student_path
    end
  end

  def update
    @student = Student.find(params[:id])
    if @student.update(student_params)
      flash[:success] = "修正しました！"
      redirect_to @student
    else
      flash[:warning] = "修正に失敗しました"
      render 'edit'
    end
  end

  def destroy
    # binding.pry
    @student = Student.find(params[:id])
    @student.destroy
    redirect_to students_path
  end

  private

    def student_params
      params.require(:student).permit(:name, :grade, :introduction, :bukatsu_id)
    end
end
