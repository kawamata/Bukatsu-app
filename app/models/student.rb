class Student < ApplicationRecord
	belongs_to :bukatsu
	validates :name, presence: true
	validates :grade, presence: true
	validates :introduction, presence: true
	validates :bukatsu_id, presence: true
end
