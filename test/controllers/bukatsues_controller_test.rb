require 'test_helper'

class BukatsuesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get bukatsues_index_url
    assert_response :success
  end

  test "should get show" do
    get bukatsues_show_url
    assert_response :success
  end

end
