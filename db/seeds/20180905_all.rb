Bukatsu.create!([
  {name: "テニス部"},
  {name: "水泳部"},
  {name: "サッカー部"},
  {name: "体操部"},
  {name: "剣道部"},
  {name: "柔道部"},
  {name: "ダンス部"},
  {name: "合気道"}
])
Student.create!([
  {name: "小太郎", grade: 2, introduction: "米大好き", bukatsu_id: 4},
  {name: "よしえ", grade: 3, introduction: "茶道に精通", bukatsu_id: 1},
  {name: "山太郎", grade: 1, introduction: "将棋が得意です", bukatsu_id: 1},
  {name: "任天堂", grade: 2, introduction: "草刈りを日課にしてます", bukatsu_id: 2},
  {name: "あかり", grade: 3, introduction: "川辺で水炊きをつくること", bukatsu_id: 3},
  {name: "あかり", grade: 3, introduction: "川辺で水炊きをつくること", bukatsu_id: 3},
  {name: "岬くん", grade: 3, introduction: "ミッドフィルダー", bukatsu_id: 3},
  {name: "大崎", grade: 2, introduction: "餅つきが待ち遠しい", bukatsu_id: 5},
  {name: "まとめん", grade: 1, introduction: "新入生だけど頑張ります", bukatsu_id: 5},
  {name: "五輪", grade: 2, introduction: "各国の五輪は見逃さない", bukatsu_id: 5},
  {name: "Vector", grade: 1, introduction: "留学生", bukatsu_id: 5},
  {name: "悠次郎", grade: 1, introduction: "明日の良い日", bukatsu_id: 5},
  {name: "あきこ", grade: 2, introduction: "頂上を目指して..", bukatsu_id: 7}
])
