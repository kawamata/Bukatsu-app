class CreateBukatsus < ActiveRecord::Migration[5.1]
  def change
    create_table :bukatsus do |t|
      t.string :name

      t.timestamps
    end
  end
end
