# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Bukatsu.create(name: "テニス部")
Bukatsu.create(name: "水泳部")
Bukatsu.create(name: "サッカー部")
Bukatsu.create(name: "体操部")
Bukatsu.create(name: "剣道部")
Bukatsu.create(name: "柔道部")
Bukatsu.create(name: "ダンス部")