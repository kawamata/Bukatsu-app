Rails.application.routes.draw do
  root 'home#top'

  get 'home/index'

  resources :students

  # get 'students/index'

  # get 'students/show'

  # get 'students/new'

  # post 'students/create'

  # post 'students/update'

  # delete 'students/destroy'

  get 'bukatsues/index'

  get 'bukatsues/show'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
